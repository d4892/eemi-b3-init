/* 
Class Shape
Define the main structure and method of a shape
@constructor{number} x: shape position x
@constructor{number} y: shape position y
@constructor{number} width: shape width
@constructor{number} heigth: shape heigth (opt.)
*/
    class Shape{
        /* 
        Class initialization
        The consturcto is used to inject values
        */
            constructor(x, y, width, heigth){
                // Use `Vector` to set position
                this.position = new p5.Vector(x, y);
                
                // Set shape width and height
                this.width = width;
                this.heigth = heigth;
            };
        //
    }
//

/* 
Class Circle
Define the design of a circle shape
@constructor{number} strokeWeight: shape stroke weight (opt.)
@constructor{number} strokeColor: shape stroke color (opt.)
@constructor{number} fillColor: shape fill color (opt.)
@constructor{number} score: shape score (opt.)
@constructor{boolean} hidden: set shape display
*/
    class Circle extends Shape {
        constructor(x, y, width, heigth) {
            // Use super function to inject Shape class constructor values
            super(x, y, width, heigth);


            // Set shape direction
            this.direction = 'right';
            this.speed = 5;
            this.velocity = 'faster';
            this.sizing = 'bigger'

            // Set shape colors
            this.strokeWeight = 5;
            this.strokeColor = `rgba(255, 0, 0, 1)`;
            this.fillColor = `rgba(0, 0, 255, 1)`;
        };

        /* 
        Method to display shape in canvas
        No param needed
        */
            display() {
                // Create circle with ellipse
                ellipseMode(CENTER)
                strokeWeight(this.strokeWeight)
                stroke(this.strokeColor)
                fill(this.fillColor)
                circle(this.position.x, this.position.y, this.width);
            };
        //

        /* 
        Method to define velocity
        */
            setMouvement(){
                // Changer la couleur de fond
                this.fillColor = randomColor(false);

                // Modifier la vitesse
                this.velocity === 'faster'
                ? this.speed++ : this.speed--;

                // Modifier la taille de la forme
                this.sizing === 'bigger'
                ? this.width += 10 : this.width -= 10;
            }
        //

        /* 
        Method to move shape
        */
            move(bounceX){
                // Vérifier la vitesse
                if(this.speed >= 50){ this.velocity = 'slower' }
                else if(this.speed <= 1){ this.velocity = 'faster' }

                // Vérifier la limite X
                if( this.position.x >= (bounceX[1] - this.width/2 + this.strokeWeight)) {
                    // Changer la direction
                    this.direction = 'left';

                    // Changer la couleur de fond
                    this.setMouvement();
                }
                else if(this.position.x <= bounceX[0] + this.width/2 + this.strokeWeight){
                    // Changer la direction
                    this.direction = 'right'

                    // Changer la couleur de fond
                    this.setMouvement()
                }

                // Déplacer la forme de ratio pixel
                this.direction === 'right'
                ? this.position.x += this.speed
                : this.position.x -= this.speed;

                // Modifier la valeur du sizing
                if(this.width >= 300){ this.sizing = 'smaller' }
                else if(this.width <= 50){ this.sizing = 'bigger' }
            }
        //  
    };
//