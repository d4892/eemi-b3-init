/*  
Déclaration de variables
*/
    let canvasWidth;
    let canvasHeight;
    let canvasColor;
    let mainCircle = null;
//

/* 
P5.js fonction setup()
Permet d'injecter des valeurs dans P5.js
*/
    function setup(){
        // Définir la largeur et la hauteur du canvas
        canvasWidth = 500;
        canvasHeight = 500;

        // Créer le canvas
        createCanvas(canvasWidth, canvasHeight);

        // Définir la couleur de fond
        canvasColor = randomColor(false)

        // Créer un cercle
        mainCircle = new Circle(canvasWidth/2, 250, 50, 50);
    }
//

/* 
P5.js fonction draw()
Permet de dessiner sur le canvas
*/
   function draw(){
       // Ajouter un ecouleur de fond
        background(canvasColor);

        // Afficher un cerlce
        mainCircle.display()

        // Déplacer le cercle
        mainCircle.move([0, canvasWidth]);
    }
//