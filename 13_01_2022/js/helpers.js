/* 
    Function to define random value
    @params{number} min: first step
    @params{number} max: last step
    @params{boolean} floor: option to set floor
*/
    function randomize(min, max, floor = false){
        // Define random value
        const rando = Math.random() * max;

        // Return random
        if(!floor){ return rando + min }
        else{ return Math.floor( rando + min ) };
    }
//

/* 
    Function to define random color
    @params{boolean} opacity: option to set opacity
*/
    function randomColor(opacity){
        return opacity
        ? `rgba(${randomize(0, 255, true)}, ${randomize(0, 255, true)}, ${randomize(0, 255, true)}, ${Math.random()})`
        : `rgba(${randomize(0, 255, true)}, ${randomize(0, 255, true)}, ${randomize(0, 255, true)}, 1)`;
    }
//